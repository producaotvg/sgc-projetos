from __future__ import print_function # Python 2/3 compatibility
import boto3
import json
import decimal
from boto3.dynamodb.conditions import Key, Attr

#resources
dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
table = dynamodb.Table('sgc-projeto')

#Incluir novo projeto
def incluirProjeto(json_projeto):

    response = table.put_item(
       Item = json_projeto,
       ConditionExpression='attribute_not_exists(nome)'
    )
    return response


#Consultar projeto por nome
def consultarProjetoPorNome(nome):
    response = table.query(
        KeyConditionExpression=Key('nome').eq(nome)
    )
    return response

#Alterar novo projeto
def alterarProjeto(json_projeto):
    response = table.update_item(
        Key={
            'nome': json_projeto['nome']
        },
        UpdateExpression="set responsavel=:0",
        ExpressionAttributeValues={
            ':0': json_projeto['responsavel']
        },
        ReturnValues="UPDATED_NEW"
    )
    return response

# Excluir novo projeto
def excluirProjeto(chave):
    response = table.delete_item(
        Key={
            'nome': chave
        }
    )
    return response
