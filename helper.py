from __future__ import print_function
import json
import os
from flask import Flask, jsonify, request, make_response, abort

# Get variavel de ambiente
def variable(name):
    try:
        name = os.environ[name]
        return name
    except KeyError:
        message  = mensagem({ 'mensagem': 'Variable does not exist.'}, 500)

        raise Exception(message)


    return os.environ[name]

# Limpar atributos vazio
def clean_attributes(data):
   data_clean = dict((k, v) for k, v in data.items() if v)
   return data_clean


# convert date to json
def date_handler(obj):
    return obj.isoformat() if hasattr(obj, 'isoformat') else obj

# Tratamento da mensagem de retorno
def mensagem(message, code):
    response =  {
        'statusCode': code,
        'body': message,
        'headers': {
            'Content-Type': 'application/json',
        },
    }
    return make_response(jsonify( response ), code)
