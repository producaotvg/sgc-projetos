# Small container for running Python with Boto3
FROM 874081334926.dkr.ecr.us-east-1.amazonaws.com/python:latest
MAINTAINER lbaiao
LABEL Version="0.1"
EXPOSE 8081

COPY . /app
WORKDIR /app

RUN pip install boto3
RUN pip install -r requirements.txt

ENTRYPOINT ["python"]
CMD ["application.py"]
