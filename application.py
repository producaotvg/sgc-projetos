#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
import json
import logging
import os
from botocore.exceptions import ClientError
from flask import Flask, jsonify, request, make_response, abort

from helper import variable, mensagem
from dao import incluirProjeto, consultarProjetoPorNome, alterarProjeto, excluirProjeto
from aws_xray_sdk.core import xray_recorder
from aws_xray_sdk.ext.flask.middleware import XRayMiddleware

#resources
application = Flask(__name__)

xray_recorder.configure(service='sgc-projetos')
XRayMiddleware(application, xray_recorder)

logger = logging.getLogger()
logger.setLevel(logging.INFO)

#variables

@application.errorhandler(400)
def not_found(error):
    return mensagem( { 'message': 'Bad request' } , 400)

# Cross origin
@application.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin','*')
    response.headers.add('Access-Control-Allow-Headers', "Authorization, Content-Type")
    response.headers.add('Access-Control-Expose-Headers', "Authorization")
    response.headers.add('Access-Control-Allow-Methods', "GET, POST, PUT, DELETE, OPTIONS")
    response.headers.add('Access-Control-Allow-Credentials', "true")
    response.headers.add('Access-Control-Max-Age', 60 * 60 * 24 * 20)
    return response


# Serviço responsavel pelo health check
@application.route("/health", methods=['GET'])
def show_health():
        return "OK", 200


# serviço responsavel pela inclusao de novos projetos
@application.route("/projeto", methods=['POST'])
def show_post():

    logger.info("Message Recieved - POST")
    logger.info('REQUEST={}'.format(request.content_type))

    try:

        if request.content_type != 'application/json':
            return mensagem( { 'message': 'O conteudo nao é json' } , 400)
        try:
            data = json.loads(request.data)
        except ValueError:
            return mensagem( { 'message': 'Você deve informar o json no corpo da requisição.' } , 400)

        if 'nome' not in request.json:
            return mensagem( { 'message': 'Você deve informar o nome do projeto' }, 400)

        if 'responsavel' not in request.json:
            return mensagem( { 'message': 'Você deve informar o nome do responsavel pelo projeto' }, 400)

        json_projeto = {
            'nome': request.json['nome'],
            'responsavel': request.json['responsavel']
        }

        response = incluirProjeto(json_projeto)

        return mensagem( { 'message': 'gravação efetuada com sucesso' } , 200)

    except ClientError as ce:
        logger.error("ClientError" , exc_info=True)
        if ce.response['Error']['Code'] == 'ConditionalCheckFailedException':
            logger.error("ConditionalCheckFailedException detected with nome=%s", request.json['nome'])
            return mensagem( { 'message': 'Já existe projeto com esse nome'} , 400)
        raise
    except Exception as e:
        logger.error("Exception" , exc_info=True)
        return mensagem( { 'message': 'Erro ao efetuar a gravação do novo projeto' }, 500)
        raise


# Serviço responsavel por consultar um projeto.
@application.route("/projeto", methods=['GET'])
def show_get():
    logger.info("Message Recieved - GET")
    logger.info('REQUEST={}'.format(request.content_type))

    try:
        if 'nome' not in request.args:
            return mensagem( { 'message': 'Você deve informar o nome do projeto que deseja consultar' } , 400)

        data = consultarProjetoPorNome(request.args['nome'])

        if data['Count'] > 0:
            return mensagem( data['Items'], 200 )
        else:
            return mensagem( { 'message': 'Não existe projeto com o nome, ' + str(request.args['nome']) } , 400)

    except Exception as e:
        logger.error("Exception" , exc_info=True)
        logger.error(e)
        return mensagem( { 'message': 'Erro ao efetuar a integração com serviços da AWS.' } , 500)
        raise


# serviço responsavel pela alteracao das informacoes do projeto
@application.route("/projeto", methods=['PUT'])
def show_put():

    logger.info("Message Recieved - PUT")
    logger.info('REQUEST={}'.format(request.content_type))

    try:

        if request.content_type != 'application/json':
            return mensagem( { 'message': 'O conteudo não é json.' }, 400)
        try:
            data = json.loads(request.data)
        except ValueError:
            return mensagem( { 'message': 'Você deve informar o json no corpo da requisição.' } , 400)

        if 'nome' not in request.json:
            return mensagem( { 'message': 'Você deve informar o nome do projeto.' }, 400)

        if 'responsavel' not in request.json:
            return mensagem( { 'message': 'Você deve informar o nome do responsável pelo projeto.' }, 400)

        # consultar
        response_consulta = consultarProjetoPorNome(request.json['nome'])
        if response_consulta ['Count'] == 0:
            return mensagem( { 'message': 'Não existe projeto com esse nome.' }, 400)
        else:

            # alterar
            json_projeto = {
                'nome': request.json['nome'],
                'responsavel': request.json['responsavel']
            }
            response = alterarProjeto(json_projeto)

        return mensagem( { 'message': 'gravação efetuada com sucesso' } , 200)

    except ClientError as ce:
        logger.error("ClientError" , exc_info=True)
        if ce.response['Error']['Code'] == 'ConditionalCheckFailedException':
            logger.error("ConditionalCheckFailedException detected with nome=%s", request.json['nome'])
            return mensagem( { 'message': 'Já existe projeto com esse nome.'}, 400)
        raise
    except Exception as e:
        logger.error("Exception" , exc_info=True)
        return mensagem( { 'message': 'Erro ao efetuar a gravação do novo projeto.'} , 500)
        raise

# Serviço responsavel por excluir um projeto.
@application.route("/projeto", methods=['DELETE'])
def show_excluir():
    logger.info("Message Recieved - DELETE")
    logger.info('REQUEST={}'.format(request.content_type))

    try:
        if 'nome' not in request.args:
            return mensagem( { 'message': 'Você deve informar o nome do projeto que deseja excluir.' } , 400)

        response_excluir = excluirProjeto(request.args['nome'])

        return mensagem( { 'message': 'Projeto excluído com sucesso.' } , 200)
    except Exception as e:
        logger.error("Exception" , exc_info=True)
        return mensagem(  { 'message': 'Erro ao efetuar a integração com serviços da AWS.' }, 500 )
        raise


if __name__ == '__main__':
    application.run(debug=False,host='0.0.0.0',port=8081)
